package test_runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.cli.Main;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		format = "pretty",
		features = "features",
		glue = "step_definitions"
		)

public class TestRunner {
	public static void main(String[] args) throws Throwable {
	Main.main(new String[]{"-g", "step_definitions", "-p", "pretty"});
	}
}
