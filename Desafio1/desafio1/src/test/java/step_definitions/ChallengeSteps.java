package step_definitions;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import challenge.Support;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;

public class ChallengeSteps {
  final static Logger logger = Logger.getLogger(ChallengeSteps.class);
  private Scenario sc;
  private Support support;
  private WebDriver driver;
  private FileAppender appender;
  private Wait<WebDriver> wait;

  @Before
  public void before(Scenario sc) {
    this.sc = sc;
  }

  @After
  public void after(Scenario sc) throws IOException {
    if(sc.isFailed()) {
      logger.info("Scenario " + sc.getName() + " is failed!!!");
      support.takeScreenshot();
    }
    support.endsScenario();
  }

  @Given("^I Open the browser$")
  public void openBrowser(List<String> list) throws IOException {
    String browser = list.get(0);
    if(browser.toUpperCase().startsWith("F")) {
      driver = new FirefoxDriver();
    }
    else if(browser.toUpperCase().startsWith("C")) {
      driver = new ChromeDriver();
    }
    driver.manage().window().maximize();
    @SuppressWarnings("deprecation")
    Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
      .withTimeout(30, TimeUnit.SECONDS)
      .pollingEvery(5, TimeUnit.SECONDS)
      .ignoring(StaleElementReferenceException.class)
      .ignoring(NoSuchElementException.class);
    
    this.wait = wait;
    support = new Support(driver, sc.getName(), System.getProperty("user.dir")+"/"+sc.getName());
    support.setAppender();
    appender = support.getAppender();
    logger.addAppender(appender);
    logger.info("Starting scenario \"" + sc.getName() + "\" on " + browser);
    
  }
}