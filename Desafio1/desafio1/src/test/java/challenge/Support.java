package challenge;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Support {
  final static Logger logger = Logger.getLogger(Support.class);
  private int screenShotCounter;
  private WebDriver driver;
  private String scenarioName;
  private List<File> files;
  private String dir;
  private FileAppender appender;

  public Support(WebDriver driver, String scenarioName, String dir) {
    this.driver = driver;
    this.scenarioName = scenarioName;
    this.dir = dir;
  }

  public void setAppender() throws IOException {
    PatternLayout layout = new PatternLayout("%-2d{dd-MM-yy HH:mm:ss} %-5p %c{1}:%L - %m%n");
    appender = new FileAppender(layout, dir + scenarioName + ".log", false);
    logger.addAppender(appender);
  }

  public void killAppender() {
    appender.finalize();
  }

  public FileAppender getAppender() {
    return appender;
  }

  public void takeScreenshot() throws IOException {
    try {
      Thread.sleep(1000);
    }
    catch (InterruptedException e) {

    }
    screenShotCounter += 1;
    File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
    File newName = new File(scenarioName + "_" + screenShotCounter + ".png");
    file.renameTo(newName);
    files.add(newName);
    file.delete();
  }
  
  public void copyScreenshot() throws IOException {
    for (File file : files)
      FileUtils.copyFileToDirectory(file, new File(dir));
    for (File file : files)
      file.delete();
  }

  public void endsScenario() throws IOException {
    logger.info("Ending \"" + scenarioName + "\" scenario");
    copyScreenshot();
    killAppender();
    driver.quit();
  }

}