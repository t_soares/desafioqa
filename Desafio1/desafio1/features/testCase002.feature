Feature: Testing search functionalities
  Scenario Outline: Validating search results not in São Paulo
    Given I Open the browser
      | <browser> |
    And I go to the site "https://www.unimed.coop.br/"
    And I click at the tab "Guia Médico"
    And I search for "Pediatra" at "Rio de Janeiro", "Rio de Janeiro"
    Then I validate the first "3" pages not in "São Paulo"

    Examples:
    | browser |
    | Firefox |
    | Chrome  |